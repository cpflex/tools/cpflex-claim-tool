/**
 *  Name:  AccountList.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { useRecoilState } from "recoil";
import Account from "../store/accounts";
import COLORS from "../theme/colors";

const Item = ({ item, onPress, backgroundColor, textColor }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
    <View>
      <Text style={[styles.itemHeader, textColor]}>{item.label}</Text>
      <Text style={[styles.itemSubtitle, textColor]}>{item.url}</Text>
    </View>
    <Text
      style={[
        {
          position: "absolute",
          right: 5,
          top: 17,
          width: 20,
          height: 20,
        },
        styles.itemHeader,
        textColor,
      ]}
    >
      {">"}
    </Text>
  </TouchableOpacity>
);

export default function AccountList( {onItemSelected}) {
  const [acctList] = useRecoilState(Account.summaries);
  const [selectedId, setSelectedId] = useRecoilState(Account.activeId);

  //Implement Item rendering.
  const renderItem = ({ item }) => {
    const backgroundColor =
      item.id === selectedId ? COLORS.background2 : COLORS.background3;
    const color = item.id === selectedId ? COLORS.labelLight : COLORS.labelDark;

    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          onItemSelected(item.label, item.id);
        }}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
  };

  //Render the Account Selection View.
  return (
    <FlatList
      style={styles.list}
      data={acctList}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      extraData={selectedId}
    />
  );
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: COLORS.controlBackground,
  },
  item: {
    padding: 10,
    marginVertical: 4,
    marginHorizontal: 4,
    borderRadius: 5,
  },
  itemHeader: {
    fontSize: 18,
    fontWeight: "700",
  },
  itemSubtitle: {
    fontSize: 12,
    fontWeight: "400",
  },
});
