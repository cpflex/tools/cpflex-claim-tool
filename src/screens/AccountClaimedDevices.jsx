/**
 *  Name:  AccountClaimedDevices.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image
} from "react-native";
import React, { Suspense, useState,useLayoutEffect, useEffect } from "react";
import { useRecoilState, useResetRecoilState} from "recoil";
import Account from '../store/accounts'
import Claims from  '../store/claims'
import COLORS from "../theme/colors";

//var ctLoad = 0;


/****************************************************************************
 * List Item Definition.
 ***************************************************************************/
const Item = ({ item, onPress, backgroundColor, textColor }) => { 


  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
      <Image
        source={Claims.getImage( item.uriImg)}
        style={ styles.itemImage}
      />
      <View style={{ marginLeft: 10, marginTop: 3 }}>
        <Text style={[styles.itemHeader, textColor]}>{'S/N ' + item.serialnumber}</Text>
      </View>
      <Text
        style={[
          {
            position: "absolute",
            right: 5,
            top: 17,
            width: 20,
            height: 20,
          },
          styles.itemHeader,
          textColor,
        ]}
      >{">"}</Text>
    </TouchableOpacity>
)};


/****************************************************************************
 * Render Functions
 ***************************************************************************/
export default function AccountClaimedDevices({navigation, route}) {
  //console.log('AccountClaimedDevices', route.params);
  const acctid = route.params.id;
  const [selectedId, setSelectedId] = React.useState(null);
  const [loadingPages, setLoadingPages] = React.useState( false);
  const [pageCurrent, setCurrentPage] = React.useState( -1);
  const [acctList] = useRecoilState( Account.summaries);
  const [claims, setClaims]   = useRecoilState( Claims.pageList);
  const [paging, setPaging] = useRecoilState( Claims.paging);
  const [claimCount, setClaimCount] = useRecoilState( Claims.countClaims);
  const resetClaims = useResetRecoilState(Claims.pageList);
  const resetPaging = useResetRecoilState(Claims.paging);
  const resetCount  = useResetRecoilState(Claims.countClaims);
  const acctinfo = Account.lookupAccount( acctid, acctList);
  const [textSearch, setTextSearch] = useState( null );
  
  //-----------------------------------------------------------
  //Initialize the page.
  //-----------------------------------------------------------

  /**
   * Processes changes in navigation state.
   */
  useEffect(() => {
    //Reset data if account  is not valid
    if( claims.length == 0 || claims[0].acctid != acctid) {
      resetClaims();
      Claims.getClaimCount(acctid, acctinfo.apikey)
      .then( count => {
        setClaimCount( count);
      })
      .catch( error => {
        console.log("Error loading claim count data: " + error);
      });      
    }
  }, [navigation]);

  useEffect( () => {
      loadMoreData();
      //console.log("CURRENT PAGE", pageCurrent);
  }, [pageCurrent]);

  useEffect( () => {
    if( route.params?.scannedKey) {
      processKey( route.params.scannedKey);
    }
  }, [route.params?.scannedKey])

  const requestMoreData= () => {
    if( !loadingPages && (pageCurrent == -1 || paging.morePages)) {
      setCurrentPage( pageCurrent+1);
    }
  }
  /**
   * Load more data as pagination occurs.
   */
   const loadMoreData = async () => {
    const bNewAccount = claims.length==0 || claims[0].acctid != acctid

    //Reset the paging if changing the accounts.  Due to batch rendering, the state 
    //may not be reset in time for fetching more data.  This forces it.
    var localPaging = Object.assign( {}, paging);
    if( bNewAccount) {
      //console.log("CLEARING LIST", acctid);
      localPaging = Claims.initPaging();
      setCurrentPage(-1);
    }

    if( !loadingPages && (pageCurrent != -1 && !localPaging.morePages))  {
      return;
    }

    //console.log("CURRENT PAGE", pageCurrent);
    setLoadingPages(true);
    try {

      //Query the additional pages.
      //console.log(`FETCH: acctid=${acctid}, apikey=${acctinfo.apikey}, page=${localPaging.pageIndex}`);
      const results = await Claims.loadPages( acctid, acctinfo.apikey, localPaging);
      const newList = (!bNewAccount) ? [...claims, ...results.list] : results.list;
      //console.log(`RESULTS: count=${results.list.length}`);
      setPaging( results.paging);
      setClaims( newList);
      setLoadingPages(false);
    } catch( error ) {
      // console.log("Error loading claims data: " + error);
      setLoadingPages(false);
    }

    //ctLoad++;
  }


  //-----------------------------------------------------------
  //Navigates to the specified claimed item.
  //-----------------------------------------------------------  
  const navigateItem = (serialnumber) => {
    setSelectedId(serialnumber);
    navigation.navigate('DeviceInfo', {title: 'S/N ' + serialnumber, serialnumber: serialnumber});
  }
  //-----------------------------------------------------------
  //Helper function to process serial and claimkeys scanned or entered on text box.
  //-----------------------------------------------------------
  const processKey = ( key) => {
    const parsed = Claims.parseClaimkeyOrDeviceUri( key);
    const claimkey = parsed.claimkey;
    var sn = claimkey.substring(0,26);
    console.log( `Searching: sn=${sn}, key=${claimkey}`);
    var claim = Claims.lookupClaim( claimkey.substring(0,16), claims);
    if( claim != undefined ) {
      navigateItem( claim.serialnumber);
    } else {
      console.log('Querying for existing claim');
      Claims.queryClaim( acctid, acctinfo.apikey, key)
      .then( claim => {
        setClaims( [claim, ...claims]);
        navigateItem( claim.serialnumber);
      })
      .catch( error => {
        console.log('Attempting to register claim');
        Claims.registerClaim( acctid, acctinfo.apikey, key) 
        .then( claim=> {
          setClaims( [claim, ...claims]);
          navigateItem( claim.serialnumber);
        })
        .catch(error=> {
          console.log( error);
        })
      });
    }
  }

  //-----------------------------------------------------------
  //Claim Item rendering in the context of the primary component.
  //-----------------------------------------------------------
  const renderItem = ({ item }) => {
    const backgroundColor =
      item.id === selectedId ? COLORS.background2 : COLORS.background3;
    const color = item.id === selectedId ? COLORS.labelLight : COLORS.labelDark;

    //List item rendering.
    return (
      <Item
        item={item}
        onPress={() => {
            navigateItem( item.serialnumber);
          }}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
  };

  const renderFooter= () => {
    return loadingPages ?( 
      <View style={styles.footer}>
        <Text>Loading...</Text>
      </View>
    ) : null;
  }

  //-----------------------------------------------------------
  //Account List and header rendering.
  //-----------------------------------------------------------
  return (
    <View style={styles.container}>
      <Suspense fallback={<Text>Loading...</Text>}>
        <Text style={styles.claim}>{"Claimed Devices: " + claimCount }</Text>
        <View style={{ flexDirection: "row", width: "98%" }}>
          <TextInput 
            style={styles.search} 
            placeholder="Enter Serial No. / ClaimKey" 
            onChangeText= {text => setTextSearch(text)}
            selectTextOnFocus={true} 
          />
          <TouchableOpacity onPress={()=> {
            
            if( Claims.validateSerialOrClaimKey(textSearch)) {
              processKey( textSearch.toUpperCase());
            } else {
              //If text search is empty, then try scanning.
              navigation.navigate({
                name: 'ScanClaimKey',
                params: {
                  apikey: acctinfo.apikey,
                  previous_screen: "AccountClaimedDevices"
                },
                merge: true
              });                
            }
          }}>
            <Image
                source={require("../assets/images/qrscan-512.png")}
                style={{ width: 32, height: 32 }}
            />
          </TouchableOpacity>
        </View>
        <FlatList
          style={styles.list}
          data={claims}
          renderItem={renderItem}
          keyExtractor={(item) => item.serialnumber}
          extraData={selectedId}
          onEndReached={requestMoreData}
          onEndReachedThreshold ={0.5}
          ListFooterComponent= {renderFooter}
        />
      </Suspense>
    </View> 
  );
}

/****************************************************************************
 * Styles
 ***************************************************************************/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.screenBackground,
    alignItems: "center",
  },
  list: {
    width: "98%",
    marginTop: 8,
    marginHorizontal: 4,
    marginVertical: 4,
    flex: 1,
    paddingTop:3,
    backgroundColor: COLORS.controlBackground,
    borderRadius: 5,
  },
  item: {
    padding: 5,
    marginVertical: 2,
    marginHorizontal: 4,
    flexDirection: "row",
    alignContent: "center",
    borderRadius: 5,
  },
  itemHeader: {
    fontSize: 18,
    fontWeight: "700",
  },

  itemImage: {
      width: 40, 
      height: 40, 
      borderRadius:5, 
      overflow:"hidden",
      borderWidth: 1,
      borderColor: COLORS.complement1Light,
      backgroundColor: "#FFF", 
  },
  search: {
    fontSize: 18,
    color: COLORS.inputText,
    flexGrow: 1,
    marginRight: 8,
    paddingVertical:2,
    paddingHorizontal: 10,
    borderWidth:1,
    borderRadius: 10,
    backgroundColor: COLORS.inputBackground,
  },
  claim: {
    fontSize: 18,
    color: '#FFF',
    padding: 8,
  },
});

/****************************************************************************
 * Utility Functions
 ***************************************************************************/
