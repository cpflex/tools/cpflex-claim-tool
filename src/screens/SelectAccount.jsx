/**
 *  Name:  SelectAccount.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
import { FlatList, StyleSheet, Text, View, Button,TouchableOpacity  } from 'react-native'
import { useRecoilState} from "recoil";
import {Suspense, useState,useLayoutEffect}  from 'react'
import AccountList from '../components/AccountList'
import Prompt from '../components/Prompt'
import Account from '../store/accounts'

import COLORS  from '../theme/colors';

const PromptState = {
  Inactive:0,
  Active:1,
  Ok:2,
  Cancel:3
}

/**
 * Encapsulates new account prompt to ensure asynchronous hooks are entirely within the scope
 * of the SelectAccount.
 * @returns React Prompt Component.
 */
const AddAccountPrompt = ({statePrompt, setPromptState}) => { 
  const [acctList] = useRecoilState( Account.summaries);
  const [selectedId, setSelectedId] = useRecoilState( Account.activeId);
  const [list, setList] = useRecoilState( Account.accountList);  
  return (   
    <Prompt
      visible={statePrompt.state == PromptState.Active}
      title="Enter API KEY"
      placeholder="CP-Flex Account API Key"
      onCancel={() =>
        setPromptState({
              state : PromptState.Cancel,
              value : ""
          })
      }
      onSubmit={ (text) => {
          //Add the new item if valid, update the list and select the new item.
          Account.addAccount( text, '.', list)
          .then( (newList) => {
            const newItem = newList[newList.length-1]; 
            setList( newList);
            setSelectedId( newItem.id);
            setPromptState({
              state : PromptState.Ok,
              value : text
            });
  
            //Now Navigate to the new item
            //navigation.navigate('AccountClaimedDevices', {title: newItem.label, id: newItem.id});
          })
          .catch( (error) => {
            console.log( 'Error adding account: ' + text + ', ' + error);
            setPromptState({
              state : PromptState.Cancel,
              value : ""
            });
          });
      }}
    />
    );
}

/**
 * Select Account Screen provides user means to select a CP-Flex Account to interact.
 * @param {Navigation} param0 
 * @returns React Component.
 */
export default function SelectAccount({navigation}) {
  const [prompt, setPrompt] = useState( {state: PromptState.Inactive, value: ""});

  //Install a button in the navigation Bar.
  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Select Account",
      headerRight: () => (
        <Button style= {styles.addButton} onPress={() =>{ 
          setPrompt( {                  
            state : PromptState.Active,
            value : ""})

        }} title="+" />
      ),
    });
  }, [navigation]);

  //Render the Account Selection View.
  return (
    <View style={styles.container} >
      <Suspense fallback={<Text>Loading...</Text>}>
        <AddAccountPrompt statePrompt={prompt} setPromptState={setPrompt} />
        <AccountList 
          onItemSelected = {(title, id)=> 
            navigation.navigate(
              'AccountClaimedDevices', 
              {
                title: title, 
                id: id
            })
          } />
      </Suspense>        
    </View>
  );
}


const styles = StyleSheet.create({
  addButton: {
    backgroundColor: COLORS.controlBackground
  },
  container: {
    flex: 1,
    padding: 2,
    backgroundColor : COLORS.screenBackground,
  }
});

