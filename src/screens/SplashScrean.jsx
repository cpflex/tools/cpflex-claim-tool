/**
 *  Name:  SplashScreen.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image} from 'react-native'
import React from 'react'
import COLORS  from '../theme/colors';

/****************************************************************************
 * Render Functions
 ***************************************************************************/

export default function SplashScreen() {
  return  (
    <View style={styles.container}>
      <Text style={styles.titleText}>Claim Tool</Text>
      <Image source="../assets/images/Cora Logo Master White.png" width="200" height="200"/>
      <StatusBar style="auto" />
  </View>
  );
}

/****************************************************************************
 * Styles
 ***************************************************************************/

const styles = StyleSheet.create({
  container: {
  flex: 1,
  backgroundColor: '#fff',
  alignItems: 'center',
  justifyContent: 'center',
  }, 
  titleText:  {
    fontSize:18,
    fontWeight: "700",
    color: COLORS.labelLight
  }
});
