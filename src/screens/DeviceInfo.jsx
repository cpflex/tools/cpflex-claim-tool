/**
 *  Name:  DeviceInfo.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

import { FlatList, StyleSheet, Text, TouchableOpacity, View, Image  } from 'react-native'
import * as  Clipboard from 'expo-clipboard';
import React from 'react'
import { useRecoilState } from "recoil";
import Claims from  '../store/claims'
import COLORS  from '../theme/colors';

/****************************************************************************
 * Render Functions
 ***************************************************************************/

/**
 * Device information Header Block.
 * @param {*} item The header data.
 * @returns 
 */
const Header = ({item}) => {
    return (
        <View style={{flexDirection:"row", justifyContent:"space-between", width:"100%"}}>
            <View style= { styles.header}>
                <Image source={ Claims.getImage(item.uriImg)} style={ styles.headerImage}/>    
                <View style={{marginLeft:6}}>
                    <Text style={styles.headerLabel}>{"Mfg. ID:"}</Text>
                    <Text style={styles.headerText}>{item.mfgid}</Text>
                    <Text style={styles.headerLabel}>{"Date Claimed:"}</Text>
                    <Text style={styles.headerText}>{item.dtclaimed}</Text>
                </View>  
            </View>
            <TouchableOpacity onPress={async ()=> {

                //Format the properties as a simple object.
                const properties= {};
                item.properties.forEach( (entry) =>{
                    properties[entry.id] = entry.value;
                });
                const newItem = {... item};
                newItem.properties = properties;

                //Copy to clip board.
                const sval = JSON.stringify( newItem, null, 2);
                console.log( sval);
                await Clipboard.setStringAsync(sval);
            }}>
                <Image 
                    
                    source={require("../assets/images/copypaste.png")}
                    style={{margin:10, width: 24, height: 26 }}
                />
          </TouchableOpacity>            
        </View>
    );
};


/**
 * Device Property Item.
 * @param {{id:string, label:string, value:string}} item The property item data.
 * @param {String} value Property item string value.
 * @returns returns the view object containing the item.
 */
const PropertyItem = ({ item }) => (
    <View style={styles.propertyItem}>
        <Text style={styles.propertyLabel}>{Claims.getLabel( item.id)}</Text>
        <Text style={styles.propertyText}>{item.value}</Text>
    </View>
);
  
/**
 * Render function for the device information screen.
 * @param {*} navigation Navigiation Object.
 * @param {*} route The Route Object.
 * @returns The screen rendering object.
 */
export default function DeviceInfo({navigation, route}) {
    //console.log(JSON.stringify( route, null, 2))

    const [pageList]   = useRecoilState( Claims.pageList);
    const claim        = Claims.lookupClaim(route.params.serialnumber, pageList );
  
    return (
        <View style={styles.container} >
            <Header item = {claim} />
            <Text style={styles.headerLabel}>{"Properties:"}</Text>
            <FlatList 
                style={styles.propertyList}
                data ={claim.properties}
                renderItem={PropertyItem}
                keyExtractor={(item)=>item.id}
            />
        </View>
    );
}

/****************************************************************************
 * Styles
 ***************************************************************************/
const styles = StyleSheet.create({
    container: {
        padding: 2,
        flex: 1,
        backgroundColor : COLORS.screenBackground,
    },
    propertyList: {
        marginTop:2,
        flex: 1,
        backgroundColor : COLORS.controlBackground,
        borderRadius: 5,
    },
    propertyItem: {

        padding: 2,
        marginVertical: 1,
        marginHorizontal: 4,
    },
    propertyLabel: {
        fontSize: 12,
        fontWeight: "700",
        color: COLORS.labelLight
    },
    propertyText: {
        fontSize: 15,
        fontWeight: "500"
    },
    header: {
        marginVertical: 10,
        marginHorizontal: 4,
        flexDirection: "row",
    },
    headerImage : {
        width:90, height:90, 
        borderRadius:5, 
        overflow:"hidden",
        borderWidth: 1,
        borderColor: COLORS.complement1Light,
        backgroundColor: "#FFF",         
    },
    headerLabel: {
        fontSize: 14,
        fontWeight: "400",
        color: COLORS.complement1Normal        
    },
    headerText: {
        fontSize:18,
        fontWeight: "700",
        color: COLORS.labelLight
    }
});