/**
 *  Name:  ScanClaimKey.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
import React, {useState, useEffect} from 'react'
import {
    StyleSheet,
    Text,
    View
    
  } from 'react-native';
import { BarCodeScanner} from 'expo-barcode-scanner';
import Claims from  '../store/claims';
import Button from '../components/button';


export default function ScanClaimKey({navigation, route}) {
   // console.log('ScanClaimKey', route.params);
    const apikey = route.params.apikey;
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [claimInfo, setClaimInfo] = useState(null);
    const [claimDevice, setClaimDevice] = useState(true);
    const [text, setText] = useState('Not yet scanned');
    const askForCameraPermission = () => {
       (async() => { 
           const {status} = await BarCodeScanner.requestPermissionsAsync();
           setHasPermission( status == 'granted');
       })()
    }

    useEffect(() =>{
        askForCameraPermission();
    }, []);
  
    const handleBarCodeScanned = async ({type, data}) => {
        setScanned(true);
        setText(data);

        //Validate that the scanned item is valid, if so,
        //Go Back and call onKeyScanned 
        if( Claims.validateSerialOrClaimKey( data) ) {
            try{
                const ci = await Claims.getClaimInfo(apikey,data);
                setClaimInfo( ci);
                switch( ci.status) {
                    case 'notfound': {
                        data += '\r\n-- not found --';
                        break;
                    } 
                    case 'available': {
                        data += '\r\n' + ci.productid + '\r\n Available (not claimed)';
                        break;
                    }
                    case 'claimed': {
                        data += '\r\n' + ci.productid + '\r\n Claimed';
                        break;
                    }
                    case 'owned': {
                        data += '\r\n' + ci.productid + '\r\n Claimed (owned by account)';
                        break;
                    }
                    case 'subaccount': {
                        data += '\r\n' + ci.productid + '\r\n Claimed (owned by sub-account)';
                        break;
                    }

                }
                setText( data);
            }
            catch( err) {
                setText(err.message);
            }
        } else { 
            setClaimInfo(null);
            setClaimDevice(false);
        }
    }

    if( hasPermission === null) {
        return (<View style={styles.container}>
            <Text>Requesting permission for Camera</Text>
        </View>
        );
    }

    if( hasPermission === false) {
        return (<View style={styles.container}>
            <Text>No access to camera</Text>
            <Button title={'Allow Camera'} onPress={() => askForCameraPermission()}/>
        </View>
        );
    }

    const canClaim = claimInfo && claimInfo.status === 'available';
    const canInfo  = claimInfo && (claimInfo.status === 'owned' || claimInfo.status === 'subaccount'); 
    return (
        <View style={styles.container}>
            <View style={styles.barcodebox}>
                <BarCodeScanner
                    onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                    style={{height: 400, width: 400}} />
            </View>
            <Text style={styles.maintext}>{text}</Text>
            {scanned && <View>
                <Button  title={'Scan Again'} onPress={() => setScanned(false)} />
                {canClaim && <Button title={'Claim Device'} onPress={() => {
                    navigation.navigate({
                        name: route.params.previous_screen, 
                        params: {scannedKey: claimInfo.claimkey},
                        merge: true
                    });
                    //onKeyScanned( claimInfo.claimkey);
                }}  style={styles.button} />}
                {canInfo && <Button title={'Show Details'} onPress={() => {
                    navigation.navigate({
                        name: route.params.previous_screen, 
                        params: {scannedKey: claimInfo.claimkey},
                        merge: true
                    });
                    //onKeyScanned( claimInfo.claimkey);
                }}  style={styles.button} />}                
                </View>
            
            }
        </View>
     );
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        flexDirection: "column",
        marginTop: 32,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    barcodebox: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 300,
        width: 300,
        overflow: 'hidden',
        borderRadius: 30,
        backgroundColor: 'tomato'
    },
    maintext: {
        fontSize: 16,
        margin: 20,
        textAlign: 'center'

    },
    button: {
        marginTop: "1px",
        marginBottom: "1px"
    }

});

