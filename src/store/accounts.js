/**
 *  Name:  accounts.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

import { atom, selector, useRecoilState } from "recoil";
import {
  asynchObjectStorageEffect,
  asynchStringStorageEffect,
} from "./asynchStorageEffect";

const CPFLEX_PROVISIONING_URL = "http://global.cpflex.tech/provisioning/v1.0";
/****************************************************************************
 * Atoms
 ***************************************************************************/

/**
 * Defines an atom containing the list of accounts.
 */
export const accountList = atom({
  key: "AccountList",
  default: [
    /*
     {
       id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba/.",
       apikey: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
       path: ".",
       label: "Codepoint Test",
       url: "TEST//resellers/codepoint/test",
     },
     {
       id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63/.",
       apikey: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
       path: ".",
       label: "Codepoint Demokits",
       url: "TEST//demokits/codepoint",
     },
     {
       id: "58694a0f-3da1-471f-bd96-145571e29d72/.",
       apikey: "58694a0f-3da1-471f-bd96-145571e29d72",
       path: ".",
       label: "Reseller #1",
       url: "TEST//resellers/reseller1",
     }*/
  ],
  effects: [asynchObjectStorageEffect("account_list")],
});

export const activeId = atom({
  key: "AccountActiveId",
  default: null,
  effects: [asynchStringStorageEffect("account_active_id")],
});

export const summaries = selector({
  key: "AccountSummaries",
  get: ({ get }) => {
    return get(accountList);
  },
});

/****************************************************************************
 * Exports
 ***************************************************************************/

/**
 * Adds an account with the specified API key to the end of the list.
 * @param {string} apikey
 * @param {string} path
 * @returns {Promise}  Returns promise for an updated account list.
 */
export function addAccount(apikey, path = ".", list) {
  return fetch(CPFLEX_PROVISIONING_URL + "/account/info", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      apikey: apikey,
    },
    body: JSON.stringify({
      acct: path,
      fullpath: true,
    }),
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        throw new Error("HTTP Error " + response.status);
      }
    })
    .then((info) => {
      //Validate account is not already in the list.
      if (
        list.length > 0 &&
        list.find((item) => item.id == info.acctid) != undefined
      ) {
        throw new Error("Duplicate account specified");
      }

      //Add it.
      const newItem = {
        id: info.acctid,
        apikey: apikey,
        path: path,
        label:
          info.label !== null && info.label.length > 0 ? info.label : info.path,
        url: info.path,
      };
      return [...list, newItem];
    })
    .catch((error) => {
      throw new Error(error);
    });
}
/**
 * Removes the specified account from the list of accounts.
 * @param {string} acctid The accout id to remove
 * @param {[]} list  The updated list.
 */
export function removeAccount(acctid, list) {
  return list.filter((item) => item.id !== acctid);
}

/**
 * Returns specified account information.
 * @param {string} acctid  The Account in the list.
 * @param {[]} list
 * @returns {*} Returns the account or undefined if not found.
 */
export function lookupAccount(acctid, list) {
  return list.find((item) => item.id == acctid);
}

/****************************************************************************
 * Internal functions
 ***************************************************************************/

module.exports = {
  accountList: accountList,
  activeId: activeId,
  summaries: summaries,
  addAccount: addAccount,
  removeAccount: removeAccount,
  lookupAccount: lookupAccount,
};
