/**
 *  Name:  claims.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

import { atom, selector, useRecoilState } from "recoil";

/****************************************************************************
 * Constants
 ***************************************************************************/

const CPFLEX_PROVISIONING_URL = "http://global.cpflex.tech/provisioning/v1.0";
const CPFLEX_CLAIMS_URL = "http://global.cpflex.tech/claims/v1.0";
const SERIAL_NUM_EXAMPLE = "XXXX-XXXXXX-XXXX";
const CLAIM_KEY_EXAMPLE = "XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX";
/**
 * Static labels to indicate the user friendly name of properties.
 */
const _propertyLabels = {
  pkguri: "Application Version:",
  firmwareuri: "Firmware Version:",
  deviceType: "Device Type:",
  deveui: "Device EUI:",
  joineui: "App/Join EUI:",
  appkey: "App Key:",
  idsubscription: "Subscription ID:",
  region: "Region:",
  frequency: "Frequency",
  "lpwan-protocol": "Comm. Protocol",
  claimkey: "Claim Key:",
};

const _images = [
  {
    id: "cora/cs1000/v1:r1-ul",
    img: require("../assets/images/CS1000-UL-128.png"),
  },
  {
    id: "cora/cs1010/v1:r1-ul",
    img: require("../assets/images/CS1010-UL-128.png"),
  },
  {
    id: "cora/cs1030/v1:r1-ul",
    img: require("../assets/images/CS1030-UL-128.png"),
  },
  {
    id: "nali/n100/v1:rev3",
    img: require("../assets/images/N100-128.png"),
  },
];

/****************************************************************************
 * Recoil Atoms and Selectors
 ***************************************************************************/

/**
 * Defines an atom containing the current page list of items.
 */
export const pageList = atom({
  key: "ClaimPageList",
  default: [],
});

export const paging = atom({
  key: "ClaimPaging",
  default: initPaging(),
});

export const countClaims = atom({
  key: "ClaimPageCount",
  default: -1,
});

export const summaries = selector({
  key: "ClaimSummaries",
  get: ({ get }) => {
    return get(pageList);
  },
});

/****************************************************************************
 * Api
 ***************************************************************************/

/**
 * Retrieves the image source.
 * @param {string} uri
 * @throws Throws an exception if invaled.
 */
export function getImage(uri) {
  const entry = _images.find((item) => item.id == uri);
  return entry != undefined ? entry.img : undefined;
}

/**
 * Returns the label corresponding to the well-known property id.
 * @param {string} id The property identifier.
 * @returns {string} The string label.
 */
export function getLabel(id) {
  const label = _propertyLabels[id];
  return label != null ? label : id;
}

/**
 * Returns specified claim info.
 * @param {string} apikey
 * @param {string} path
 * @param {[]} list
 * @returns {*} Returns the account or undefined if not found.
 */
export function lookupClaim(serialnumber, list) {
  return list.find((item) => item.serialnumber == serialnumber);
}

export function initPaging(sizePage = 20, index = -1) {
  return {
    limit: -1,
    pageIndex: index,
    pageSize: sizePage,
    state: [],
  };
}

/**
 * Attempts to query existing device.  Will return device information or throw
 * exception if failed.
 * @param {*} acctid
 * @param {*} apikey
 * @param {string} key  The serial number or claimkey
 * @returns {Promise<*>} Returns a promise for a claimed object.
 */
export function queryClaim(acctid, apikey, key) {
  return fetch(CPFLEX_CLAIMS_URL + "/registrar/info", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      apikey: apikey,
    },
    body: JSON.stringify({
      acct: acctid,
      key: key,
    }),
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else if (response.status == 404) {
        throw new Error("Device claim not found in account");
      } else {
        throw new Error("HTTP Error " + response.status);
      }
    })
    .then((results) => {
      return formatClaimEntry(results);
    });
}

/**
 *
 * @param {*} acctid
 * @param {*} apikey
 * @returns {Promise<*>} Returns the count of claims for an account.
 */
export function getClaimCount(acctid, apikey) {
  return fetch(CPFLEX_CLAIMS_URL + "/registrar/count", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      apikey: apikey,
    },
    body: JSON.stringify({
      acct: acctid,
    }),
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else if (response.status == 400) {
        throw new Error("Account not found");
      } else {
        throw new Error("HTTP Error " + response.status);
      }
    })
    .then((output) => {
      //console.log("response: " + JSON.stringify(output, null, 2));
      return output.count;
    });
}

/**
 * Attempts to register claim key.  Will return device information or throw
 * exception if failed.
 * @param {*} acctid
 * @param {*} apikey
 * @param {string} claimkey  The claimkey
 * @returns {Promise<*>} Returns a promise for a claimed object.
 */
export function registerClaim(acctid, apikey, claimkey) {
  return fetch(CPFLEX_CLAIMS_URL + "/registrar/claim", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      apikey: apikey,
    },
    body: JSON.stringify({
      acct: acctid,
      claimkey: claimkey,
    }),
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else if (response.status == 400) {
        throw new Error("Device not found");
      } else {
        throw new Error("HTTP Error " + response.status);
      }
    })
    .then((results) => {
      return formatClaimEntry(results);
    });
}

/**
 * Returns the claimkey information.
 * @param {*} apikey
 * @param {string} key
 * @returns { Promise<{claimkey:string, status:string, productid: string}>}
 */
export function getClaimInfo(apikey, key) {
  return fetch(CPFLEX_CLAIMS_URL + "/registrar/check", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      apikey: apikey,
    },
    body: JSON.stringify({
      claimkeys: [key],
    }),
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        throw new Error("HTTP Error " + response.status);
      }
    })
    .then((results) => {
      return results[0];
    });
}

/**
 * Processes claimkey or Device URI.
 * @param {string} claimkey parses the claimkey or deviceuri.
 *  claimkey : XXXX-XXXXXX-XXXX-XXXXXXXXXXXXXXXXXX
 *  deviceuri : <manufacturer>/<productid>/<claimkey>[?<metadata>]
 * @returns { {
 *  claimkey: string,
 *  serialnumber: string,
 *  claimsecret: string
 * }} Returns an object comprising claimkey and its parts properly formatted.
 */
function parseClaimkeyOrDeviceUri(claimkey) {
  //If device URI format is detected, extract the claimkey and process normally.
  if ((claimkey && claimkey.indexOf("/") >= 0) || claimkey.indexOf("?") >= 0) {
    const e1 = claimkey.split("?");
    const e2 = e1[0].split("/"); //Grab the left or first element containing the path.
    if (e2.length < 3) {
      throw new ClaimError(
        "invalid device URI format detected, must be standard <manufacturer>/<productid>/<claimkey>[?<metadata>"
      );
    }
    claimkey = e2[2].toUpperCase();
  }

  //Make sure we are upper case by definition.
  claimkey = claimkey.toUpperCase();

  //Test the claim key format.
  if (!validateClaimkey(claimkey)) {
    throw new Error("Claim key format is not valid: " + claimkey);
  }

  //Return the serialnumber and claim secret.
  return {
    claimkey: claimkey,
    serialnumber: claimkey.substring(0, SERIAL_NUM_EXAMPLE.length),
    claimsecret: claimkey.substring(SERIAL_NUM_EXAMPLE.length + 1),
  };
}

/**
 * Tests the specified claimkey for proper format.
 * @param {string} claimkey
 * @returns {boolean} True if valid, false otherwise.
 */
function validateClaimkey(claimkey) {
  var bValid = typeof claimkey == "string" && claimkey.length == 35;
  for (var i = 0; bValid && i < 35; i++) {
    const val = claimkey.charAt(i);
    bValid =
      ((i == 4 || i == 11 || i == 16) && val == "-") ||
      (val >= "0" && val <= "9") ||
      (val >= "A" &&
        val <= "Z" &&
        val != "I" &&
        val != "L" &&
        val != "O" &&
        val != "U");
  }

  return bValid;
}

/**
 * Returns true if key is deemed a Codepoint serial number or claim key.
 * @param {string} key
 * @returns
 */
export function validateSerialOrClaimKey(key) {
  const bIsClaimOrSerial =
    key != null &&
    key.length >= 16 &&
    key[4] == "-" &&
    key[11] == "-" &&
    (key.length == 16 || (key.length == 35 && key[16] == "-"));

  const bIsUri = key != null && key.indexOf("/") > 0;

  return bIsClaimOrSerial || bIsUri;
}

/**
 * Loads the specified page and returns a promise for the page list.
 * @param {*} acctid
 * @param {*} apikey
 * @param {*} paging
 */
export function loadPages(acctid, apikey, paging) {
  const body = JSON.stringify({
    acct: acctid,
    verbose: true,
    paging: paging,
  });
  //console.log("API request: " + body);
  return fetch(CPFLEX_CLAIMS_URL + "/registrar/list", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      apikey: apikey,
    },
    body: body,
  })
    .then((response) => {
      if (response.status == 200) {
        return response.json();
      } else {
        throw new Error("HTTP Error " + response.status);
      }
    })
    .then((results) => {
      let output = {
        list: [],
        paging: results.paging,
      };

      const info = {
        claimcount: results.claims.length,
        paging: results.paging,
        morePages: results.morePages,
      };
      //console.log("API response: \r\n" + JSON.stringify(info, null, 2));
      //Construct the summary list for the UI.
      if (results.claims.length > 0) {
        results.claims.forEach((claim) =>
          output.list.push(formatClaimEntry(claim))
        );
      }
      return output;
    })
    .catch((error) => {
      throw new Error(error);
    });
}

/****************************************************************************
 * Module Internal Functions
 ***************************************************************************/

/**
 * Transform claim data into a claim entry and returns it.
 * @param {*} claim
 * @returns
 */
function formatClaimEntry(claim) {
  //get the Manufacturing ID
  const idMfg = claim.identities.find((item) => item.id == "mfgid");
  var mfgid = "";
  if (idMfg != null) {
    mfgid = idMfg.value;
  }

  //Get the unique part of the device type.
  const strKey = "device-type/";
  const idx = claim.devicetype.indexOf(strKey);
  var devicetype = "";
  if (idx > 0) {
    devicetype = claim.devicetype.substring(idx + strKey.length);
  }

  //Create an entry that is a subset of the full entry.
  let entry = {
    serialnumber: claim.serialnumber,
    acctid: claim.acctid,
    dtclaimed: claim.dtclaimed,
    mfgid: mfgid,
    uriImg: devicetype,
    properties: [
      { id: "pkguri", value: getTag(claim.pkguri) },
      { id: "firmwareuri", value: getTag(claim.firmwareuri) },
      { id: "deviceType", value: devicetype },
      { id: "deveui", value: "" },
      { id: "joineui", value: "" },
      { id: "appkey", value: "" },
      { id: "idsubscription", value: claim.idsubscription },
      { id: "region", value: "" },
      { id: "frequency", value: "" },
      { id: "lpwan-protocol", value: "" },
      { id: "claimkey", value: claim.claimkey },
    ],
  };

  //Identities (not mfgid or chipuuid).
  claim.identities.forEach((item) => {
    if (item.id != "mfgid" && item.id != "chipuuid") {
      const idx = entry.properties.findIndex((elem) => elem.id == item.id);
      if (idx < 0) {
        entry.properties.push(item);
      } else {
        entry.properties[idx].value = item.value;
      }
    }
  });

  //Attributes
  for (const [key, value] of Object.entries(claim.attributes)) {
    const prop = { id: key, value: value };
    const idx = entry.properties.findIndex((elem) => elem.id == key);
    if (idx < 0) {
      entry.properties.push(prop);
    } else {
      entry.properties[idx].value = value;
    }
  }
  return entry;
}

function getTag(uri) {
  if (uri == null) {
    return "- N/A -";
  }
  const idx = uri.indexOf(":");
  if (idx > 0) {
    return uri.substring(idx + 1);
  }
}

/****************************************************************************
 * Module Exports.
 ***************************************************************************/
module.exports = {
  pageList: pageList,
  paging: paging,
  countClaims: countClaims,
  summaries: summaries,
  getImage: getImage,
  getLabel: getLabel,
  getClaimCount: getClaimCount,
  lookupClaim: lookupClaim,
  initPaging: initPaging,
  queryClaim: queryClaim,
  registerClaim: registerClaim,
  parseClaimkeyOrDeviceUri: parseClaimkeyOrDeviceUri,
  validateSerialOrClaimKey: validateSerialOrClaimKey,
  loadPages: loadPages,
  getClaimInfo: getClaimInfo,
};
