/**
 *  Name:  asyncStorageEffect.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

import { DefaultValue } from "recoil";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const asynchObjectStorageEffect =
  (key) =>
  ({ setSelf, onSet }) => {
    setSelf(
      AsyncStorage.getItem(key).then((savedValue) =>
        savedValue != null ? JSON.parse(savedValue) : new DefaultValue()
      )
    );

    // Subscribe to state changes and persist them to AsynchStorage.
    onSet((newValue, _, isReset) => {
      isReset
        ? AsyncStorage.removeItem(key)
        : AsyncStorage.setItem(key, JSON.stringify(newValue));
    });
  };

export const asynchStringStorageEffect =
  (key) =>
  ({ setSelf, onSet }) => {
    setSelf(
      AsyncStorage.getItem(key).then((savedValue) =>
        savedValue != null ? savedValue : new DefaultValue()
      )
    );

    // Subscribe to state changes and persist them to AsynchStorage.
    onSet((newValue, _, isReset) => {
      isReset
        ? AsyncStorage.removeItem(key)
        : AsyncStorage.setItem(key, newValue);
    });
  };
