/**
 *  Name:  colors.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const COLORS = {
  primaryNormal: "#0F152B",
  primaryLight: "#2E4185",
  primaryDark: "#0F152B",
  complement1Normal: "#C28E33",
  complement1Dark: "#9E742A",
  complement1Light: "#F5B341",
  complement2Normal: "#3E8A46",
  complement2Dark: "#204724",
  complement2Light: "#66E373",
  highlight: "#FFF",
  highlightLight: "#FFF",
  highlightDark: "#9E742A",
  background1: "#FFF",
  background2: "#0F152B",
  background3: "#DDDDDD",
  header1: "#C28E33",
  header2: "#0F152B",
  header3: "#0F152B",
  body: "#0F152B",
  labelLight: "#FFF",
  labelDark: "#0F152B",
  controlBackground: "#AAA",
  inputText: "#0F152B",
  inputTextHint: "#F5B341",
  inputBackground: "#FFF",
  screenBackground: "#0F152B",
};

export default COLORS;
