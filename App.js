/**
 *  Name:  app.jsx
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

import * as React from "react";
import { Text } from "react-native";
import { RecoilRoot } from "recoil";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { LogBox } from "react-native";

LogBox.ignoreLogs(["Setting a timer"]);

import SelectAccount from "./src/screens/SelectAccount";
import AccountClaimedDevices from "./src/screens/AccountClaimedDevices";
import DeviceInfo from "./src/screens/DeviceInfo";
import ScanClaimKey from "./src/screens/ScanClaimKey";
import COLORS from "./src/theme/colors";
const Stack = createNativeStackNavigator();

function App() {
  return (
    <RecoilRoot>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SelectAccount">
          <Stack.Screen
            name="SelectAccount"
            component={SelectAccount}
            options={{
              title: "Select Account",
              headerStyle: {
                backgroundColor: COLORS.complement1Normal,
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="AccountClaimedDevices"
            component={AccountClaimedDevices}
            options={({ route }) => ({
              title: route.params.title,
              headerStyle: {
                backgroundColor: COLORS.complement1Normal,
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            })}
          />
          <Stack.Screen
            name="DeviceInfo"
            component={DeviceInfo}
            options={({ route }) => ({
              title: route.params.title,
              headerStyle: {
                backgroundColor: COLORS.complement1Normal,
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            })}
          />
          <Stack.Screen
            name="ScanClaimKey"
            component={ScanClaimKey}
            options={{
              title: "Scan Claim Key",
              headerStyle: {
                backgroundColor: COLORS.complement1Normal,
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </RecoilRoot>
  );
}

export default App;
